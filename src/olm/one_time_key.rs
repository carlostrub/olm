use super::OneTimeKey;
use rand::rngs::OsRng;
use std::fmt;

impl fmt::Debug for OneTimeKey {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("IdentityKeys")
            .field("id", &self.id)
            .field("published", &self.published)
            .field("curve25519", &self.curve25519_public)
            .finish()
    }
}

impl Default for OneTimeKey {
    fn default() -> Self {
        let mut csprng = OsRng {};

        let c_secret = x25519_dalek::EphemeralSecret::new(&mut csprng);
        let c_public = x25519_dalek::PublicKey::from(&c_secret);

        OneTimeKey {
            id: 0,
            published: false,
            curve25519_secret: c_secret,
            curve25519_public: c_public,
        }
    }
}
