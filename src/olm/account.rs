use super::{Account, IdentityKeys, OneTimeKey};
use std::cmp::Ordering;
use std::fmt;
use std::hash::{Hash, Hasher};
use uuid::Uuid;

/// Implements methods for the Account struct
impl Account {
    /// Get Alice's identity keys. Be careful with this method as it also returns the private key!
    fn identity_keys(&self) -> &IdentityKeys {
        &self.identity_keys
    }

    /// Return the identifier of an Account
    pub fn id(&self) -> &String {
        &self.id
    }

    /// Change the ID of an account
    pub fn with_id(self, id: String) -> Account {
        Account { id, ..self }
    }

    /// Change the Keypair of an account. This is for example used if a keypair is loaded from
    /// storage.
    fn with_identity_keys(self, identity_keys: IdentityKeys) -> Account {
        let mut ok = Vec::new();
        ok.push(OneTimeKey::default());
        // TODO: invalidate old OneTimeKey properly

        Account {
            identity_keys,
            one_time_keys: ok,
            ..self
        }
    }

    /// Create a new Account
    ///
    /// # Example
    ///
    /// ```
    /// use olm::Account;
    /// let account = Account::new().with_id("@cs:carlostrub.ch".to_string());
    ///
    /// assert_eq!(format!("This is account: {}", account), "This is account: (@cs:carlostrub.ch)");
    /// ```
    pub fn new() -> Self {
        Account::default()
    }
}

/// If Default is called, a random ID is generated, as starting with an empty id string might be
/// risky. If implementing Default, it is strongly recommended to alter the id to something
/// reasonable and recognizable, e.g. e-mail address, etc.
impl Default for Account {
    fn default() -> Self {
        let uuid = (*Uuid::new_v4()
            .to_hyphenated()
            .encode_lower(&mut Uuid::encode_buffer()))
        .to_string();

        let mut ok = Vec::new();
        ok.push(OneTimeKey::default());

        Account {
            id: uuid,
            identity_keys: IdentityKeys::default(),
            one_time_keys: ok,
        }
    }
}

/// PartialEq is implemented by comparing public keys and ids
impl PartialEq for Account {
    fn eq(&self, other: &Self) -> bool {
        self.identity_keys().eq(other.identity_keys()) && self.id() == other.id()
    }
}

/// Eq is implemented using empty impl block
impl Eq for Account {}

/// Ord is implemented using the id of Accounts
impl Ord for Account {
    fn cmp(&self, other: &Self) -> Ordering {
        self.id.cmp(&other.id)
    }
}

/// PartialOrd is using cmp implemented in Ord trait
impl PartialOrd for Account {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Hash is implemented by hashing id and identity keys
impl Hash for Account {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.identity_keys().hash(state);
    }
}

/// Display shows the id
impl fmt::Display for Account {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({})", self.id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_account_new_1() {
        use ed25519_dalek::Signature;
        let conv = Account::new();
        let uuid = Uuid::parse_str(conv.id()).unwrap();

        let message: &[u8] = b"This is a test of the tsunami alert system.";
        let signature: Signature = conv.keypair().sign(message);
        assert!(conv.keypair().verify(message, &signature).is_ok());
        assert_eq!(uuid.get_version_num(), 4);
    }

    #[test]
    fn test_account_new_2() {
        use ed25519_dalek::Signature;
        let conv1 = Account::new();
        let conv1_keypair = conv1.keypair().to_bytes();
        let conv2 = Account::new().with_keypair(Keypair::from_bytes(&conv1_keypair).unwrap());

        let message: &[u8] = b"This is a test of the tsunami alert system.";
        let signature: Signature = conv2.keypair().sign(message);
        assert!(conv1.keypair().verify(message, &signature).is_ok());
    }

    #[test]
    fn test_account_eq() {
        let conv1 = Account::new().with_id("a".to_string());
        let conv2 = Account::new().with_id("a".to_string());

        assert_eq!(conv1, conv1);
        assert_ne!(conv1, conv2);
    }

    #[test]
    fn test_account_cmp() {
        let conv1 = Account::new().with_id("a".to_string());
        let conv2 = Account::new().with_id("b".to_string());

        assert_eq!(conv1.cmp(&conv2), Ordering::Less);
        assert_eq!(conv1.cmp(&conv1), Ordering::Equal);
        assert_eq!(conv2.cmp(&conv1), Ordering::Greater);
        assert_eq!(conv2.partial_cmp(&conv1), Some(Ordering::Greater));
    }

    #[test]
    fn test_account_fmt() {
        let conv = Account::new().with_id("a".to_string());

        assert_eq!(format!("This is account: {}", conv), "This is account: (a)");
    }
}
