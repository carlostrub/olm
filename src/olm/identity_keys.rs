use super::IdentityKeys;
use ed25519_dalek;
use rand::rngs::OsRng;
use std::fmt;
use std::hash::{Hash, Hasher};
use x25519_dalek;

impl IdentityKeys {
    /// Return the two public keys of the identity keys, i.e. the ed25519 and the curve25519 public
    /// keys
    fn public(&self) -> (&ed25519_dalek::PublicKey, &x25519_dalek::PublicKey) {
        (&self.ed25519_public, &self.curve25519_public)
    }
}

impl fmt::Debug for IdentityKeys {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("IdentityKeys")
            .field("ed25519", &self.ed25519_public)
            .field("curve25519", &self.curve25519_public)
            .finish()
    }
}

/// PartialEq is implemented by comparing public keys and ids
impl PartialEq for IdentityKeys {
    fn eq(&self, other: &Self) -> bool {
        let p = self.public();
        let o = other.public();
        p.0 == o.0 && p.1.as_bytes() == o.1.as_bytes()
    }
}

/// Hash is implemented by hashing the byte version of the public identity keys
impl Hash for IdentityKeys {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let k = self.public();
        k.0.as_bytes().hash(state);
        k.1.as_bytes().hash(state);
    }
}

/// Eq is implemented using empty impl block
impl Eq for IdentityKeys {}

impl Default for IdentityKeys {
    fn default() -> Self {
        let mut csprng = OsRng {};

        let ed = ed25519_dalek::Keypair::generate(&mut csprng);

        let c_secret = x25519_dalek::EphemeralSecret::new(&mut csprng);
        let c_public = x25519_dalek::PublicKey::from(&c_secret);

        IdentityKeys {
            ed25519_secret: ed.secret,
            ed25519_public: ed.public,
            curve25519_secret: c_secret,
            curve25519_public: c_public,
        }
    }
}
