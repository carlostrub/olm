use ed25519_dalek;
use x25519_dalek;

/// There are two keypairs required as identity keys, an ED25519 and a Curve25519 keypair.
struct IdentityKeys {
    ed25519_secret: ed25519_dalek::SecretKey,
    ed25519_public: ed25519_dalek::PublicKey,
    curve25519_secret: x25519_dalek::EphemeralSecret,
    curve25519_public: x25519_dalek::PublicKey,
}

/// Onetime keys are used to permit others to contact us individually
struct OneTimeKey {
    id: u32,
    published: bool,
    curve25519_secret: x25519_dalek::EphemeralSecret,
    curve25519_public: x25519_dalek::PublicKey,
}

/// Holds identity information about Alice
#[derive(Debug)]
pub struct Account {
    /// Stores public identity information used to help the counterparty identify Alice. This is
    /// necessary to avoid unknown-key sharing attacks, as described in the specification.
    /// Recommended are unique recognizable strings, e.g. e-mail address.
    id: String,
    /// Identifies Alice's account and is used to sign session keys.
    identity_keys: IdentityKeys,
    one_time_keys: Vec<OneTimeKey>,
}

/// Handles the creation and updating of Accounts
pub mod account;

mod identity_keys;
mod one_time_key;
