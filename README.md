[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Carlo-Strub_olm&metric=alert_status)](https://sonarcloud.io/dashboard?id=Carlo-Strub_olm)

# Olm: A Cryptographic Ratchet
This crate is a pure Rust implementation of the *Olm* and *Megolm* cryptographic ratchets:

 - [Olm](https://gitlab.matrix.org/matrix-org/olm/-/blob/master/docs/olm.md)
 - [Megolm](https://gitlab.matrix.org/matrix-org/olm/blob/master/docs/megolm.md)
 
 These two algorithms are used in [[matrix]](https://matrix.org/) for end-to-end encryption but
 this crate offers generic Rust APIs to implement them everywhere.

**!!!DO NOT USE THIS CODE FOR PRODUCTION! IT IS NOT YET TESTED AGAINST THE REFERENCE IMPLEMENTATION!!!**

## Discussions and Support
Any support is very welcome. Please use the following matrix room to discuss topics around this
crate: [#olm:carlostrub.ch](https://riot.im/app/#/room/#olm:carlostrub.ch)

## Source Code Integrity
All commits are signed with the following GPG key (find the respective key for example in the
    [FreeBSD keyring](https://www.freebsd.org/doc/pgpkeyring.txt)):

`3626 000C 0372 A78C 5DD7  B096 34EF 3FF3 3C29 811A`

You can verify the integrity of the code by running:

`git log --show-signature`


